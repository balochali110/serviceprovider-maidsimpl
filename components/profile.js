import React from "react";
import Image from "next/image";
import PersonalInformation from "./profile-personal";

const Profile = () => {
  return (
    <>
      <div className="ml-64 pt-44">
        <div className="flex bg-[#8cd790] mr-56 rounded-lg pt-8 pb-8 border border-black">
          <div className="w-1/6 pl-12 flex place-content-end">
            <Image
              src="/icons/avatar.png"
              alt=""
              width="140"
              height="140"
              className="border border-4 border-white"
              style={{ borderRadius: "50%" }}
            />
          </div>
          <div className="w-3/5">
            <span>
              <p className="font-bold text-white text-2xl mt-8 ml-10">
                Liam Hale
              </p>
              <p className="ml-10 mt-2 flex text-gray-50">
                <Image src="/icons/usa.png" alt="" width="25" height="25" />
                <span className="ml-2"> Orlando, United States</span>
              </p>
            </span>
          </div>
        </div>
        <PersonalInformation />
      </div>
    </>
  );
};

export default Profile;
