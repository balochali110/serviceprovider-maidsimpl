import React from "react";
import Image from "next/image";
const ViewDetails = () => {
  return (
    <div className="ml-64 pt-44 mb-44">
      <div className="flex bg-[#8cd790] mr-56 rounded-lg pt-8 pb-8 border border-2 border-[#285741]">
        <div className="w-1/6 pl-12 flex place-content-end">
          <Image
            src="/icons/avatar.png"
            alt=""
            width="140"
            height="140"
            className="border border-4 border-white"
            style={{ borderRadius: "50%" }}
          />
        </div>
        <div className="w-3/5">
          <span>
            <p className="font-bold text-white text-2xl mt-8 ml-10">
              Liam Hale
            </p>
            <p className="ml-10 mt-2 flex text-gray-50">
              <Image src="/icons/usa.png" alt="" width="25" height="25" />
              <span className="ml-2"> Orlando, United States</span>
            </p>
          </span>
          <span className="">
            <p className="text-base font-medium ml-10 mt-2 text-white">
              johnsmith@gmail.com
            </p>
            <p className="text-base font-medium ml-10 mt-2 text-white">
              +11234567
            </p>
          </span>
        </div>
      </div>
      <div className="mt-12 border border-2 border-[#8cd790] rounded-lg mr-56 pb-12">
        <p className="bg-[#8cd790] pl-12 text-2xl font-medium text-white pt-4 pb-4">
          About
        </p>
        <div className="pt-12 pl-12 pr-72">
          <p className="text-lg font-medium">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium
            illo nesciunt, modi beatae fuga dolorum hic cum ipsum quibusdam
            similique sequi accusamus, rerum quos tempora distinctio dolores.
            Sed, odit temporibus.
            <br />
            <br />
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium
            illo nesciunt, modi beatae fuga dolorum hic cum ipsum quibusdam
            similique sequi accusamus, rerum quos tempora distinctio dolores.
            Sed, odit temporibus.
          </p>
        </div>
      </div>
      <div className="mt-12 border border-2 border-[#8cd790] rounded-lg mr-56 pb-12">
        <p className="bg-[#8cd790] pl-12 text-2xl font-medium text-white pt-4 pb-4">
          Review
        </p>
        <div className="pt-12 pl-12 mr-12">
          <div className="flex">
            <p className="text-lg font-medium pr-36">
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Praesentium illo nesciunt, modi beatae fuga dolorum hic cum ipsum
              quibusdam similique sequi accusamus, rerum quos tempora distinctio
              dolores. Sed, odit temporibus.
              <br />
              <br />
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Praesentium illo nesciunt, modi beatae fuga dolorum hic cum ipsum
              quibusdam similique sequi accusamus, rerum quos tempora distinctio
              dolores. Sed, odit temporibus.
            </p>
            <div className="ml-12 mr-36 mt-16">
              <div className="flex place-content-center mt-2">
                <Image
                  src="/icons/star.png"
                  width="25"
                  height="25"
                  alt=""
                  className="ml-2"
                />
                <Image
                  src="/icons/star.png"
                  width="25"
                  height="25"
                  alt=""
                  className="ml-2"
                />
                <Image
                  src="/icons/star.png"
                  width="25"
                  height="25"
                  alt=""
                  className="ml-2"
                />
                <Image
                  src="/icons/star.png"
                  width="25"
                  height="25"
                  alt=""
                  className="ml-2"
                />
                <Image
                  src="/icons/star.png"
                  width="25"
                  height="25"
                  alt=""
                  className="ml-2"
                />
              </div>
            </div>
          </div>
          <div className="border-b-2 mb-2 border-gray-500 mt-12 w-full" />
          <div className="flex mt-12">
            <p className="text-lg font-medium pr-36">
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Praesentium illo nesciunt, modi beatae fuga dolorum hic cum ipsum
              quibusdam similique sequi accusamus, rerum quos tempora distinctio
              dolores. Sed, odit temporibus.
              <br />
              <br />
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Praesentium illo nesciunt, modi beatae fuga dolorum hic cum ipsum
              quibusdam similique sequi accusamus, rerum quos tempora distinctio
              dolores. Sed, odit temporibus.
            </p>
            <div className="ml-12 mr-36 mt-16">
              <div className="flex place-content-center mt-2">
                <Image
                  src="/icons/star.png"
                  width="25"
                  height="25"
                  alt=""
                  className="ml-2"
                />
                <Image
                  src="/icons/star.png"
                  width="25"
                  height="25"
                  alt=""
                  className="ml-2"
                />
                <Image
                  src="/icons/star.png"
                  width="25"
                  height="25"
                  alt=""
                  className="ml-2"
                />
                <Image
                  src="/icons/star.png"
                  width="25"
                  height="25"
                  alt=""
                  className="ml-2"
                />
                <Image
                  src="/icons/star.png"
                  width="25"
                  height="25"
                  alt=""
                  className="ml-2"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ViewDetails;
