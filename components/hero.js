import Profile from "./profile";

const Hero = () => {
  return (
    <>
      <div className="pt-20 bg-white pb-36">
        <Profile />
      </div>
    </>
  );
};

export default Hero;
