import React from "react";
import { motion } from "framer-motion";
import ModalComponent from "./ModalComponent";

const ProfileUpdate = (props) => {
  return (
    <div>
      <ModalComponent
        isOpen={props.modalToggle}
        onClose={props.onCloseModal}
        width={600}
      >
        <div className="mt-12 w-[600px]">
          <div className="ml-12">
            <form action="">
              <div className="flex">
                <span>
                  <p className="text-lg font-medium text-gray-500">
                    First Name
                  </p>
                  <input
                    type="text"
                    className="border border-2 h-12 w-56 rounded-lg mt-2 border-[#8cd790]"
                  />
                </span>
                <span className="ml-12">
                  <p className="text-lg font-medium text-gray-500">Last Name</p>
                  <input
                    type="text"
                    className="border border-2 h-12 w-56 rounded-lg mt-2 border-[#8cd790]"
                  />
                </span>
              </div>
              <div className="flex mt-6">
                <span>
                  <p className="text-lg font-medium text-gray-500">
                    Email Address
                  </p>
                  <input
                    type="text"
                    className="border border-2 h-12 w-56 rounded-lg mt-2 border-[#8cd790]"
                  />
                </span>
                <span className="ml-12">
                  <p className="text-lg font-medium text-gray-500">Phone</p>
                  <input
                    type="text"
                    className="border border-2 h-12 w-56 rounded-lg mt-2 border-[#8cd790]"
                  />
                </span>
              </div>
              <div className="flex mt-6">
                <span>
                  <p className="text-lg font-medium text-gray-500">Country</p>
                  <input
                    type="text"
                    className="border border-2 h-12 w-56 rounded-lg mt-2 border-[#8cd790]"
                  />
                </span>
                <span className="ml-12">
                  <p className="text-lg font-medium text-gray-500">
                    City/State
                  </p>
                  <input
                    type="text"
                    className="border border-2 h-12 w-56 rounded-lg mt-2 border-[#8cd790]"
                  />
                </span>
              </div>
              <div className="flex mt-6">
                <span>
                  <p className="text-lg font-medium text-gray-500">
                    Postal Code
                  </p>
                  <input
                    type="text"
                    className="border border-2 h-12 w-56 rounded-lg mt-2 border-[#8cd790]"
                  />
                </span>
              </div>
            </form>
            <div className="mt-12 flex place-content-center mr-12 mb-4">
              <motion.button
                className="px-48 bg-[#4a4a48] py-2 text-white font-medium text-lg rounded-full"
                whileTap={{ scale: 0.7 }}
              >
                Save
              </motion.button>
            </div>
          </div>
        </div>
      </ModalComponent>
    </div>
  );
};

export default ProfileUpdate;
