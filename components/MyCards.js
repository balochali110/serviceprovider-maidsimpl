import React from "react";
import Image from "next/image";
import { motion } from "framer-motion";

const MyCards = () => {
  return (
    <div className="pt-44 bg-[#f7f7f7] pb-12 flex">
      <div className="ml-64 w-1/2">
        <p className="text-3xl font-medium font-roboto">My Cards</p>
        <div className="mt-12 bg-white rounded-[15px] w-2/3 px-4 py-4 shadow-lg text-xl font-base flex border-2 border-[#8cd790]">
          <span className="w-full pl-2">
            <p>Bank of America</p>
          </span>
          <div className="flex place-content-end w-full pr-4">
            <p># 123456789</p>
          </div>
        </div>
        <div className="mt-6 bg-white rounded-[15px] w-2/3 px-4 py-4 shadow-lg text-xl font-base flex border-2 border-[#8cd790]">
          <span className="w-full pl-2">
            <p>Bank of America</p>
          </span>
          <div className="flex place-content-end w-full pr-4">
            <p># 123456789</p>
          </div>
        </div>
        <div className="mt-6 bg-white rounded-[15px] w-2/3 px-4 py-4 shadow-lg text-xl font-base flex border-2 border-[#8cd790]">
          <span className="w-full pl-2">
            <p>Bank of America</p>
          </span>
          <div className="flex place-content-end w-full pr-4">
            <p># 123456789</p>
          </div>
        </div>

        <div className="mt-16">
          <p className="text-2xl font-medium font-roboto">Add New</p>
          <div className="mt-6">
            <p className="text-lg font-roboto">Card Number</p>
            <input
              type="text"
              className="rounded-[10px] shadow border border-[#8cd790] mt-1 w-96 pl-4 py-3 bg-[#D7EDD8]"
            />
          </div>
          <div className="mt-6">
            <p className="text-lg font-roboto">Card Holder Number</p>
            <input
              type="text"
              className="rounded-[10px] shadow border border-[#8cd790] mt-1 w-96 pl-4 py-3 bg-[#D7EDD8]"
            />
          </div>
          <div className="flex mt-6">
            <div>
              <p className="text-lg font-roboto">Expiration Date</p>
              <div className="flex">
                <input
                  type="text"
                  className="rounded-[10px] shadow border border-[#8cd790] mt-1 w-16 pl-4 py-3 bg-[#D7EDD8]"
                />
                <input
                  type="text"
                  className="rounded-[10px] ml-4 shadow border border-[#8cd790] mt-1 w-16 pl-4 py-3 bg-[#D7EDD8]"
                />
              </div>
            </div>
            <div className="ml-36">
              <p className="text-lg font-roboto">CVV</p>
              <input
                type="text"
                className="rounded-[10px] shadow border border-[#8cd790] mt-1 w-24 pl-4 py-3 bg-[#D7EDD8]"
              />
            </div>
          </div>
          <div className="mt-12">
            <motion.button
              className="bg-[#333333] text-white w-96 rounded-[15px] font-medium py-2 text-lg"
              whileTap={{ scale: 0.5 }}
            >
              Add
            </motion.button>
          </div>
        </div>
      </div>
      <div className="-ml-48 mt-24">
        <Image src="/icons/cards.png" width={700} height={500} alt="" />
      </div>
    </div>
  );
};

export default MyCards;
