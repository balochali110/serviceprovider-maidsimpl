import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import Select from "react-select";
import ModalComponent from "./Modal";
import OrderInfo from "./OrderInfo";

const BookingHistory = () => {
  const router = useRouter();

  const [serviceSelected, setSelectedService] = useState("");
  const serviceOptions = [
    { value: "pending", label: "Pending" },
    { value: "ongoing", label: "Ongoing" },
    { value: "completed", label: "Completed" },
  ];

  const rowsData = [
    {
      id: 1,
      date: "12/11/2023",
      address: "Los Angeles",
      service: "lorem ipsum",
      team: "lorem ipsum",
      payment: "$ 300",
    },
    {
      id: 2,
      date: "12/11/2023",
      address: "Los Angeles",
      service: "lorem ipsum",
      team: "lorem ipsum",
      payment: "$300",
    },
    {
      id: 3,
      date: "12/11/2023",
      address: "Los Angeles",
      service: "lorem ipsum",
      team: "lorem ipsum",
      payment: "$300",
    },
    {
      id: 4,
      date: "12/11/2023",
      address: "Los Angeles",
      service: "lorem ipsum",
      team: "lorem ipsum",
      payment: "$300",
    },
    {
      id: 5,
      date: "12/11/2023",
      address: "Los Angeles",
      service: "lorem ipsum",
      team: "lorem ipsum",
      payment: "$300",
    },
    {
      id: 6,
      date: "12/11/2023",
      address: "Los Angeles",
      service: "lorem ipsum",
      team: "lorem ipsum",
      payment: "$300",
    },
  ];

  const [modaltoggle, setModalToggle] = useState(false);
  const [data, setData] = useState({});
  const handleModalToggle = (data) => {
    setModalToggle(true);
    setData(data);
  };

  const onCloseModal = () => {
    setModalToggle(false);
  };

  return (
    <>
      <div
        className="flex place-content-center h-full pt-44 pb-12 bg-[#F7F7F7]"
        style={{ fontFamily: "roboto" }}
      >
        <div>
          <div className="flex place-content-center">
            <p className="text-center uppercase ml-48 text-3xl text-[#8cd790] font-bold mb-8 w-5/6">
              Booking History
            </p>
            <div className="flex place-content-end w-1/6">
              <span>
                <Select
                  value={serviceSelected}
                  onChange={setSelectedService}
                  options={serviceOptions}
                  className="border border-1 border-[#8cd790] rounded-lg w-44"
                  id="1"
                />
              </span>
            </div>
          </div>
          <div className="mb-12 bg-white w-full rounded-3xl drop-shadow">
            <div className="flex place-content-center">
              <div className="w-5/6 mt-5">
                <div className="relative overflow-x-auto"></div>
                <table className="w-full mt-4 text-sm text-left text-gray-500 dark:text-gray-400 border mb-8">
                  <thead className="text-base text-white uppercase">
                    <tr className="bg-[#8cd790] ">
                      <th
                        scope="col"
                        className="font-normal px-6 py-3 w-[250px]"
                      >
                        DATE
                      </th>
                      <th
                        scope="col"
                        className="font-normal px-6 py-3 w-[350px]"
                      >
                        ADDRESS
                      </th>
                      <th
                        scope="col"
                        className="font-normal px-6 py-3 w-[250px]"
                      >
                        SERVICE
                      </th>
                      <th
                        scope="col"
                        className="font-normal px-6 py-3 w-[250px]"
                      >
                        TEAM ASSIGNED
                      </th>
                      <th
                        scope="col"
                        className="font-semibold px-6 py-3 w-[250px]"
                      >
                        PAYMENT
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {rowsData.map((row) => {
                      return (
                        <>
                          <tr
                            className="bg-white border-b text-base dark:bg-gray-800 cursor-pointer hover:bg-gray-100"
                            onClick={() => handleModalToggle(row)}
                            key={row.id}
                          >
                            <td className="px-6 py-4">{row.date}</td>
                            <td className="px-6 py-4 w-1/5">{row.address}</td>
                            <td className="px-6 py-4">{row.service}</td>
                            <td className="px-6 py-4">{row.team}</td>
                            <td
                              className="px-6 py-4 text-lg font-semibold text-[#8cd790]"
                              style={{ fontFamily: "roboto" }}
                            >
                              {row.payment}
                            </td>
                          </tr>
                        </>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <OrderInfo
        modaltoggle={modaltoggle}
        onCloseModal={onCloseModal}
        data={data}
        buttonHide="hidden"
      />
    </>
  );
};

export default BookingHistory;
