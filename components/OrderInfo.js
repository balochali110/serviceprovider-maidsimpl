import React from "react";
import { motion } from "framer-motion";
import Image from "next/image";
import ModalComponent from "./ModalComponent";

const OrderInfo = (props) => {
  return (
    <>
      <ModalComponent
        isOpen={props.modaltoggle}
        onClose={props.onCloseModal}
        width={633}
        buttonHide={props.buttonHide}
      >
        <div
          className="mt-8 pb-8"
          style={{
            borderRadius: "10px",
          }}
        >
          <div
            className="w-[760px] bg-[#8cd790]"
            style={{
              borderTopLeftRadius: "10px",
              borderTopRightRadius: "10px",
            }}
          >
            <div>
              <p className="text-center pt-4 pb-4 text-3xl text-white font-medium ">
                Order Info
              </p>
              <div className={`flex place-content-end -mt-14 pb-6 mr-4`}>
                <button className="" onClick={props.onCloseModal}>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="white"
                    className="w-8 h-8"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M9.75 9.75l4.5 4.5m0-4.5l-4.5 4.5M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
                    />
                  </svg>
                </button>
              </div>
            </div>
          </div>
          <div className="mt-16 ml-12 mr-12">
            <div className="flex w-full border-b-2 pl-5 border-gray-400">
              <p className="text-lg font-medium w-5/6">ID Number:</p>
              <p className="text-lg font-medium">123456</p>
            </div>
            <div className="flex w-full border-b-2 pl-5 border-gray-400 mt-3">
              <p className="text-lg font-medium w-5/6">Client Name:</p>
              <p className="text-lg font-medium">Micheal</p>
            </div>
            <div className="flex w-full border-b-2 pl-5 border-gray-400 mt-3">
              <p className="text-lg font-medium w-5/6">Location:</p>
              <p className="text-lg font-medium">Lorem</p>
            </div>
            <div className="flex w-full border-b-2 pl-5 border-gray-400 mt-3">
              <p className="text-lg font-medium w-5/6">Date:</p>
              <p className="text-lg font-medium">01/01/2023</p>
            </div>
            <div className="flex place-content-center mt-12">
              <div className="">
                <span>
                  <Image
                    src="/icons/bedroom.png"
                    width={100}
                    height={105}
                    alt=""
                  />
                </span>
                <p className="text-center font-medium mt-3">2 Bedrooms</p>
              </div>
              <div className="ml-24">
                <span>
                  <Image
                    src="/icons/kitchen.png"
                    width={80}
                    height={105}
                    alt=""
                  />
                </span>
                <p className="text-center font-medium mt-2">1 Kitchen</p>
              </div>
              <div className="ml-24">
                <span>
                  <Image
                    src="/icons/bathroom.png"
                    width={80}
                    height={105}
                    alt=""
                    className="mt-4"
                  />
                </span>{" "}
                <p className="text-center font-medium -mt-2">2 Washrooms</p>
              </div>
            </div>
            <div className="mt-8 border-t-2 -ml-6 -mr-6 border-gray-500">
              <div className="flex place-content-center">
                <span className="w-4/6 mt-6 text-gray-400">
                  <p className="">
                    you will recieve a confirmation email after your order has
                    been accepted by a service provider.
                  </p>
                </span>
              </div>
            </div>
            <div className="w-full flex place-content-end">
              <span className="flex">
                <p className="text-xl font-medium mt-1">Total</p>
                <p
                  className="text-[#8cd790] ml-6 text-3xl font-semibold"
                  style={{
                    fontFamily: "roboto",
                  }}
                >
                  $100
                </p>
              </span>
            </div>
            <div className="flex place-content-end mt-6">
              <motion.button
                className="bg-[#4a4a48] px-10 py-2 text-white font-semibold rounded-lg"
                whileTap={{ scale: 0.7 }}
              >
                Cancel
              </motion.button>
            </div>
          </div>
        </div>
      </ModalComponent>
    </>
  );
};

export default OrderInfo;
