import Link from "next/link";
import Image from "next/image";
import React from "react";
import { useRouter } from "next/router";

export default function Footer() {
  const router = useRouter();
  const token = router.query.token;

  const handleLogin = () => {
    router.push("/login");
  };

  const handleContact = () => {
    router.push("/contact");
  };

  const handleAbout = () => {
    router.push("/aboutus");
  };

  const handleBook = () => {
    router.push("/bookus");
  };

  const handlePrivacy = () => {
    router.push("/privacy");
  };

  const handleTerms = () => {
    router.push("/terms");
  };
  return (
    <>
      <div className="w-full bg-[#8cd790] h-6 mb-3 rounded-t-full" />
      <div className="flex border-b-2 border-black mb-12 ml-40 mr-40 pb-4" style={{
        fontFamily: "roboto"
      }}>
        <div className="mt-6 mb-4 ml-8 w-1/3">
          <Image src="/img/main-logo.png" width="200" height="200" alt="" />
          <p className="font-medium mt-4 text-lg">
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Amet ipsum
            quaerat facilis repudiandae.
          </p>
          <div className="mt-4">
            <Image src="/icons/icon3.png" width="100" height="100" alt="" />
          </div>
        </div>
        <div className="ml-24 mt-12">
          <div className="flex">
            <p
              className="text-xl text-[#8cd790] font-medium cursor-pointer ml-12 border-b-2 border-transparent hover:border-b-2 hover:border-[#8cd790]"
              onClick={handleLogin}
            >
              Login
            </p>
            <p
              className="text-xl text-[#8cd790] font-medium cursor-pointer ml-12 border-b-2 border-transparent hover:border-b-2 hover:border-[#8cd790]"
              onClick={handleContact}
            >
              Contact Us
            </p>
            <p
              className="text-xl text-[#8cd790] font-medium cursor-pointer ml-12 border-b-2 border-transparent hover:border-b-2 hover:border-[#8cd790]"
              onClick={handleAbout}
            >
              About
            </p>
            <p
              className="text-xl text-[#8cd790] font-medium cursor-pointer ml-12 border-b-2 border-transparent hover:border-b-2 hover:border-[#8cd790]"
              onClick={handleBook}
            >
              Book Now
            </p>
            <p
              className="text-xl text-[#8cd790] font-medium cursor-pointer ml-12  border-b-2 border-transparent hover:border-b-2 hover:border-[#8cd790]"
              onClick={handlePrivacy}
            >
              Privacy Policy
            </p>
            <p
              className="text-xl text-[#8cd790] font-medium cursor-pointer ml-12 border-b-2 border-transparent hover:border-b-2 hover:border-[#8cd790]"
              onClick={handleTerms}
            >
              Terms & Conditions
            </p>
          </div>
          <div className="mt-24 ml-12">
            <div className="flex ">
              <Image
                src="/icons/address.png"
                width="15"
                height="18"
                alt=""
                className="h-6 mt-3"
              />
              <p className="text-base ml-4">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                <br /> Debitis quis pariatur soluta, cum quos
              </p>
            </div>
            <div className="flex mt-4">
              <Image
                src="/icons/phone.png"
                width="15"
                height="18"
                alt=""
                className="h-4 mt-1"
              />
              <p className="text-base ml-4">202-555-4458</p>
            </div>
          </div>
        </div>
      </div>
      <div className="w-full flex">
        <div className="ml-44 mb-12 tracking-widest font-medium w-1/2">
          2021 MAIDSIMPL ALL RIGHT RESERVED
        </div>
        <div className="flex w-1/2 place-content-end mr-44  ">
          <Image
            width="25"
            height="25"
            src="/icons/linkedin.png"
            alt=""
            className="ml-3 h-7 w-7"
          />
          <Image
            width="25"
            height="25"
            src="/icons/facebook.png"
            alt=""
            className="ml-3 h-7 w-7"
          />
          <Image
            width="25"
            height="25"
            src="/icons/instagram.png"
            alt=""
            className="ml-3 h-7 w-7"
          />
        </div>
      </div>
    </>
  );
}
