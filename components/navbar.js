import Link from "next/link";
import Image from "next/image";
import { useState, useEffect } from "react";
import { motion } from "framer-motion";
import { useRouter } from "next/router";

const Navbar = (props) => {
  const router = useRouter();

  const handleLogout = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("email");
    window.location.reload();
  };

  const [bgColor, setBgColor] = useState("");
  useEffect(() => {
    if (router.pathname == "/home" || router.pathname == "/profile") {
      setBgColor("white");
    } else if (
      router.pathname == "/dashboard" ||
      router.pathname == "/bookinghistory" || 
      router.pathname == "/cards" 
    ) {
      setBgColor("whitesmoke");
    } 
  }, [bgColor]);

  const Login = (e) => {
    e.preventDefault();
    setTimeout(() => {
      router.push({ pathname: "/login" });
    }, 500);
  };

  const handleFaq = () => {
    if (router.pathname === "/home") {
      const element = document.getElementById("faq");
      element.scrollIntoView({ behavior: "smooth" });
    } else {
      router.push({
        pathname: "/home",
      });

      setTimeout(() => {
        const element = document.getElementById("faq");
        element.scrollIntoView({ behavior: "smooth" });
      }, 1000);
    }
  };
  const [profile, setProfile] = useState(false);
  const handleOpen = () => {
    setProfile(!profile);
  };
  const handleAbout = () => {
    router.push({
      pathname: "/aboutus",
    });
  };
  const handleContact = () => {
    router.push("/contact");
  };
  const pathname = router.pathname;

  const handleScrollOrClick = () => {
    setProfile(false);
  };

  useEffect(() => {
    // Add event listeners for scroll and click events
    window.addEventListener("scroll", handleScrollOrClick);

    return () => {
      // Clean up event listeners
      window.removeEventListener("scroll", handleScrollOrClick);
    };
  }, []);

  const handleBookings = () => {
    router.push("/bookinghistory");
  };

  const handleProfile = () => {
    router.push("/profile");
  };

  return (
    <>
      <div
        className="relative z-50"
        style={{
          fontFamily: "roboto",
          backgroundColor: bgColor,
        }}
      >
        <div
          className={`fixed w-full pl-12`}
          style={{
            backgroundColor: bgColor,
          }}
        >
          <motion.div
            className={`container relative flex flex-wrap items-end justify-between p-8 mx-auto z-50`}
            animate={{
              y: [-250, 30, 0, 15, 0, 7, 0],
              transition: {
                duration: 3.0,
                times: [2.0, 2.2, 2.4, 2.6, 2.8, 3.0],
              },
            }}
          >
            <Link href="/">
              <span>
                <Image
                  src="/img/main-logo.png"
                  alt="N"
                  width="600"
                  height="400"
                  className="w-56"
                />
              </span>
            </Link>
            {/* menu  */}
            <div className="hidden text-center lg:flex lg:items-end">
              <ul className="items-end justify-end flex-1 list-none lg:pt-0 lg:flex">
                <li className="mr-3 nav__item">
                  <Link
                    href={{
                      pathname: "/home",
                    }}
                    className={
                      pathname === "/home"
                        ? "inline-block px-4 py-2 text-lg text-[#8CD790] font-bold no-underline rounded-md dark:text-gray-200 hover:text-[#8CD790]"
                        : "inline-block px-4 py-2 text-lg font-light text-[#4D4D4D] no-underline rounded-md dark:text-gray-200 hover:text-[#8CD790]"
                    }
                  >
                    HOME
                  </Link>
                </li>
                <li className="mr-3 nav__item">
                  <button
                    onClick={handleFaq}
                    className={
                      pathname === "/faq"
                        ? "inline-block px-4 py-2 text-lg text-[#4D4D4D] font-bold no-underline rounded-md dark:text-gray-200 hover:text-[#8CD790]"
                        : "inline-block px-4 py-2 text-lg font-light text-[#4D4D4D] no-underline rounded-md dark:text-gray-200 hover:text-[#8CD790]"
                    }
                  >
                    FAQ
                  </button>
                </li>
                <li className="mr-3 nav__item">
                  <button
                    onClick={handleAbout}
                    className={
                      pathname === "/aboutus"
                        ? "inline-block px-4 py-2 text-lg text-[#8CD790] font-bold no-underline rounded-md dark:text-gray-200 hover:text-[#8CD790]"
                        : "inline-block px-4 py-2 text-lg font-light text-[#4D4D4D] no-underline rounded-md dark:text-gray-200 hover:text-[#8CD790]"
                    }
                  >
                    ABOUT
                  </button>
                </li>
                <li className="mr-3 nav__item">
                  <Link
                    href={{
                      pathname: "/bookus",
                    }}
                    className={
                      pathname === "/bookus"
                        ? "inline-block px-4 py-2 text-lg text-[#8CD790] font-bold no-underline rounded-md dark:text-gray-200 hover:text-[#8CD790]"
                        : "inline-block px-4 py-2 text-lg font-light text-[#4D4D4D] no-underline rounded-md dark:text-gray-200 hover:text-[#8CD790]"
                    }
                  >
                    BOOK US
                  </Link>
                </li>
                <li className="mr-3 nav__item">
                  <button
                    onClick={handleContact}
                    className={
                      pathname === "/contact"
                        ? "inline-block px-4 py-2 text-lg text-[#8CD790] font-bold no-underline rounded-md dark:text-gray-200 hover:text-[#8CD790]"
                        : "inline-block px-4 py-2 text-lg font-light text-[#4D4D4D] no-underline rounded-md dark:text-gray-200 hover:text-[#8CD790]"
                    }
                  >
                    CONTACT
                  </button>
                </li>
              </ul>
            </div>
            <div className="hidden mr-3 space-x-4 lg:flex nav__item">
              {props.loggedIn ? (
                <>
                  <div className="flex rounded-full cursor-pointer">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="#8cd790"
                      className="w-12 h-12"
                      onClick={handleOpen}
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M17.982 18.725A7.488 7.488 0 0012 15.75a7.488 7.488 0 00-5.982 2.975m11.963 0a9 9 0 10-11.963 0m11.963 0A8.966 8.966 0 0112 21a8.966 8.966 0 01-5.982-2.275M15 9.75a3 3 0 11-6 0 3 3 0 016 0z"
                      />
                    </svg>
                  </div>
                </>
              ) : (
                <motion.button
                  onClick={(e) => Login(e)}
                  className="px-8 py-2 text-black font-bold rounded-md border border-1 border-black md:ml-5 hover:bg-[#8CD790] hover:text-white"
                  whileTap={{ scale: 0.7 }}
                >
                  LOGIN
                </motion.button>
              )}
            </div>
          </motion.div>
          {profile ? (
            <>
              <div className="flex place-content-end">
                <div
                  className="relative z-50 border-2 border-[#8cd790] w-36 pt-4 pb-4 pl-4 -mt-8 mr-[205px] bg-white fixed"
                  style={{
                    borderRadius: "20px",
                  }}
                >
                  <p
                    className="border-b mr-4 font-medium pb-2 hover:bg-[#8cd790] pt-2 pl-2 hover:text-white cursor-pointer rounded-lg"
                    onClick={handleBookings}
                  >
                    Bookings
                  </p>
                  <p
                    className="border-b mr-4 font-medium pb-2 pt-2 hover:bg-[#8cd790] pt-2 hover:text-white cursor-pointer pl-2 rounded-lg"
                    onClick={handleProfile}
                  >
                    My Profile
                  </p>
                  <p
                    className="border-b mr-4 font-medium pb-2 pt-2 hover:bg-[#8cd790] pt-2 hover:text-white cursor-pointer pl-2 rounded-lg"
                    onClick={handleLogout}
                  >
                    Logout
                  </p>
                </div>
              </div>
              <div className="-mt-[127px] bg-white" />
            </>
          ) : (
            ""
          )}
        </div>
      </div>
    </>
  );
};

export default Navbar;
