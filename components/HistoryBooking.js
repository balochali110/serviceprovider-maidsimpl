import React, { useState } from "react";
import Image from "next/image";
import WeeklyBarChart from "./Chart";
import Select from "react-select";

const HistoryBooking = () => {
  const chartData = [
    {
      values: [10, 15, 7, 9, 10, 20, 5],
    },
    {
      values: [8, 12, 5, 4, 4, 8, 21],
    },
    {
      values: [13, 14, 22, 4, 8, 4, 21],
    },
    {
      values: [8, 12, 5, 4, 4, 8, 21],
    },
  ];

  const [monthSelect, setMonth] = useState("");
  const month = [
    { value: "monthly", label: "Monthly" },
    { value: "weekly", label: "Weekly" },
    { value: "annually", label: "Annually" },
  ];

  const arr = [1, 2, 3, 4, 5, 6, 7];
  return (
    <div className="pt-44 bg-[#F7F7F7] pb-14 relative z-10">
      <p
        className="text-3xl font-medium text-center"
        style={{ fontFamily: "roboto" }}
      >
        Total Balance
      </p>
      <p
        className="text-7xl font-bold mt-4 text-center text-[#8cd790]"
        style={{ fontFamily: "jost" }}
      >
        $ 12,373
      </p>
      <div className="flex place-content-center mt-8">
        <Image
          src="/icons/withdraw.png"
          height={35}
          width={35}
          alt=""
          className="bg-[#8cd790] px-2 py-2 rounded-full"
        />
      </div>
      <div className="">
        <p className="mt-2 text-xl font-medium text-center text-[#4A4A4A]">
          Withdraw
        </p>
      </div>
      <div className="flex place-content-center">
        <div className="w-4/6">
          <p className="mt-12 text-2xl font-medium">Transaction History</p>
          <div className="w-6/6 flex mt-8 ml-4">
            <div className="w-1/6">
              <p className="text-lg font-medium">Date</p>
            </div>
            <div className="w-1/6">
              <p className="text-lg font-medium">Action</p>
            </div>
            <div className="w-1/6">
              <p className="text-lg font-medium">Amount</p>
            </div>
          </div>
          <div className="mt-2 bg-white w-6/6 rounded-[10px] shadow flex">
            <div className="w-1/6 py-6 px-4">
              <p className="text-lg font-medium">1-1-2023</p>
            </div>
            <div className="w-1/6 py-6 px-4">
              <p className="text-lg font-medium text-red-500">Withdrawal</p>
            </div>
            <div className="w-1/6 py-6 px-4">
              <p className="text-lg font-medium">$100</p>
            </div>
            <div className="w-1/6 py-6 px-4">
              <p className="text-lg font-medium">Nem Istan</p>
            </div>
          </div>
          <div className="mt-6 bg-white w-6/6 rounded-[10px] shadow flex">
            <div className="w-1/6 py-6 px-4">
              <p className="text-lg font-medium">1-1-2023</p>
            </div>
            <div className="w-1/6 py-6 px-4">
              <p className="text-lg font-medium text-[#8cd790]  ">Deposit</p>
            </div>
            <div className="w-1/6 py-6 px-4">
              <p className="text-lg font-medium">$100</p>
            </div>
            <div className="w-1/6 py-6 px-4">
              <p className="text-lg font-medium">Nem Istan</p>
            </div>
          </div>
          <div className="mt-6 bg-white w-6/6 rounded-[10px] shadow flex">
            <div className="w-1/6 py-6 px-4">
              <p className="text-lg font-medium">1-1-2023</p>
            </div>
            <div className="w-1/6 py-6 px-4">
              <p className="text-lg font-medium text-red-500">Withdrawal</p>
            </div>
            <div className="w-1/6 py-6 px-4">
              <p className="text-lg font-medium">$100</p>
            </div>
            <div className="w-1/6 py-6 px-4">
              <p className="text-lg font-medium">Nem Istan</p>
            </div>
          </div>
        </div>
      </div>
      <div className="ml-80 mt-16 flex mr-80">
        <p className="font-medium text-3xl w-1/2">Montly Earnings</p>
        <div className="flex place-content-end w-1/2">
          <Select
            value={monthSelect}
            onChange={setMonth}
            options={month}
            className="w-56 font-semibold shadow-lg text-xl text-center font-roboto"
            id="1"
          />
        </div>
      </div>
      <div className="ml-80 mt-2 flex rounded-lg shadow-xl bg-[#4a4a4a] w-32 h-16">
        <p
          className="text-4xl pl-3 tracking-wide pt-3 text-white font-bold text-white"
          style={{
            fontFamily: "Jost",
          }}
        >
          $ 535
        </p>
      </div>
      <div className="flex place-content-center mt-12">
        <WeeklyBarChart data={chartData} />
      </div>
      <div className="mt-7">
        <div className="ml-[350px] flex">
          <div className="w-[232px] border-l-2 border-r-2 h-4 border-b-2 border-black">
            <div className="flex place-content-center">
              <p className="text-lg  pb-2 border-none w-20 bg-[#F7F7F7] text-center font-medium font-roboto">
                Week 1
              </p>
            </div>
          </div>
          <div className="w-[232px] border-l-2 border-r-2 h-4 border-b-2 border-black ml-[100px]">
            <div className="flex place-content-center">
              <p className="text-lg  pb-2 border-none w-20 bg-[#F7F7F7] text-center font-medium font-roboto">
                Week 2
              </p>
            </div>
          </div>
          <div className="w-[235px] border-l-2 border-r-2 h-4 border-b-2 border-black ml-[100px]">
            <div className="flex place-content-center">
              <p className="text-lg  pb-2 border-none w-20 bg-[#F7F7F7] text-center font-medium font-roboto">
                Week 2
              </p>
            </div>
          </div>
          <div className="w-[232px] border-l-2 border-r-2 h-4 border-b-2 border-black ml-[100px]">
            <div className="flex place-content-center">
              <p className="text-lg  pb-2 border-none w-20 bg-[#F7F7F7] text-center font-medium font-roboto">
                Week 2
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="mt-16 ml-80">
        <p className="font-medium text-3xl">Recent Earnings</p>
        <div className="mt-12 flex">
          {arr.map((e) => {
            return (
              <div
                className="bg-white px-2 py-4 rounded-[15px] drop-shadow shadow-xl pl-8 pr-8 mr-9"
                key={e}
              >
                <span>
                  <Image width={35} src="/icons/bag.png" height={35} alt="" />
                </span>
                <p className="text-lg mt-2" style={{ fontFamily: "Jost" }}>
                  1/12/2023
                </p>
                <p
                  className="text-3xl text-[#8cd790] font-semibold mt-2"
                  style={{ fontFamily: "Jost" }}
                >
                  $ 535
                </p>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default HistoryBooking;
