import React, { useState } from "react";
import Image from "next/image";
import { useRouter } from "next/router";
import { motion } from "framer-motion";
import OTPModal from "./Modal";

const Signup = () => {
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const [modalLoading, setModalLoading] = useState(false);
  const [name, setName] = useState("");
  const [preferredName, setPreferredName] = useState("");
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");
  const [confirm, setConfirm] = useState("");
  const [otpError, setOtpError] = useState(false);

  const [error, setError] = useState(false);
  const [passError, setPassError] = useState(false);

  const [modaltoggle, setModalToggle] = useState(false);
  const onCloseModal = () => setModalToggle(false);

  //otp information
  const [otp, setOtp] = useState("");
  const [otp1, setOtp1] = useState("");
  const [otp2, setOtp2] = useState("");
  const [otp3, setOtp3] = useState("");
  const [otp4, setOtp4] = useState("");

  const handleSubmit = async (e) => {
    setLoading(true);
    e.preventDefault();
    const data = {
      name: name,
      email: email,
      preferredName: preferredName,
      password: pass,
    };

    if (
      data.name === "" ||
      data.preferredName === "" ||
      data.email === "" ||
      data.pass === "" ||
      data.confirm === ""
    ) {
      setError(true);
      setLoading(false);
    } else {
      setError(false);
      if (data.password != confirm) {
        setPassError(true);
        setLoading(false);
      } else {
        const otp = Math.floor(1000 + Math.random() * 9000);
        const message = {
          email: email,
          message:
            "This OTP is Sent From MailBox, " +
            otp +
            " Do not share it with anyone",
        };
        const sendOTP = await fetch("/api/sendOtp", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(message),
        });
        const sendOTPResponse = await sendOTP.json();
        console.log(sendOTPResponse);
        if (sendOTPResponse.success == true) {
          setPassError(false);
          setLoading(false);
          setModalToggle(true);
          setOtp(otp);
        } else {
          setOtpError(true);
          setLoading(false);
        }
      }
    }
  };

  const handleOtpCheck = async (e) => {
    e.preventDefault();
    setModalLoading(true);
    setTimeout(async () => {
      const data = {
        name: name,
        email: email,
        preferredName: preferredName,
        password: pass,
      };

      const OTP = otp1 + otp2 + otp3 + otp4;
      if (OTP == parseInt(otp)) {
        console.log("matched");
        const getSign = await fetch("/api/signup", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(data),
        });
        const response = await getSign.json();
        if (response.result === "User Created Successfully") {
          router.push("/login");
        }
      }
    }, 2000);
  };

  const handleLogin = () => {
    router.push("/login");
  };

  return (
    <>
      <div className="pl-44 pt-12 bg-[#F7F7F7] pb-[90px]">
        <Image
          src="/img/main-logo.png"
          alt="N"
          width="400"
          height="300"
          className="w-56 cursor-pointer"
          onClick={() => router.push("/home")}
        />
        <div className="flex mt-12 pl-12">
          <div>
            <p className="text-5xl text-gray-800 tracking-widest font-semibold">
              Hello
            </p>
            <p className="text-xl mt-5">Create an account, it's free.</p>
            <form action="">
              <div className="mt-10">
                <input
                  type="text"
                  placeholder="Full name"
                  className="pl-4 pr-4 border border-2 border-gray-300 text-lg bg-transparent w-96 rounded-full h-12"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                />
              </div>
              <div className="mt-6">
                <input
                  type="text"
                  placeholder="Preferred name"
                  className="pl-4 pr-4 border border-2 border-gray-300 text-lg bg-transparent w-96 rounded-full h-12"
                  value={preferredName}
                  onChange={(e) => setPreferredName(e.target.value)}
                />
              </div>
              <div className="mt-6">
                <input
                  type="text"
                  placeholder="Email"
                  className="pl-4 pr-4 border border-2 border-gray-300 text-lg bg-transparent w-96 rounded-full h-12"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </div>
              <div className="mt-6">
                <input
                  type="password"
                  placeholder="Password"
                  className="pl-4 pr-4 border border-2 border-gray-300 text-lg bg-transparent w-96 rounded-full h-12"
                  value={pass}
                  onChange={(e) => setPass(e.target.value)}
                />
              </div>
              <div className="mt-6">
                <input
                  type="password"
                  placeholder="Re-enter Password"
                  className="pl-4 pr-4 border border-2 border-gray-300 text-lg bg-transparent w-96 rounded-full h-12"
                  value={confirm}
                  onChange={(e) => setConfirm(e.target.value)}
                />
              </div>
              <div className="w-5/6 ml-7 mt-4 pt-4 pb-4 rounded-[16px] bg-white shadow-lg">
                <div>
                  <div className="flex place-content-center">
                    <div className="rounded-full bg-[#8cd790] w-10 px-2 py-2">
                      <Image
                        width={25}
                        height={25}
                        src="/icons/card.png"
                        alt=""
                      />
                    </div>
                    <p
                      className="text-center ml-4 mt-[7px] font-semibold text-lg"
                      style={{
                        fontFamily: "roboto",
                      }}
                    >
                      Government ID
                    </p>
                  </div>
                  <div className="mt-4">
                    <p className="text-center text-sm">Take a driver's license</p>
                    <p className="text-center text-sm">
                      National Identity card or passport photo
                    </p>
                  </div>
                  <div className="flex place-content-center mt-6 cursor-pointer">
                    <span>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className="w-6 h-6"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"
                        />
                      </svg>
                    </span>
                    <p className="ml-2 font-bold ">Add Documents</p>
                  </div>
                </div>
              </div>
            </form>
            <p className="mt-4 font-medium">
              Already have an account?{" "}
              <span
                className="text-[#8cd790] ml-2 cursor-pointer"
                onClick={handleLogin}
              >
                Log in
              </span>
            </p>
            <div className="flex place-content-center mt-5">
              <motion.button
                className="px-32 py-4 text-center text-xl text-white rounded-full bg-[#4A4A48] font-semibold"
                onClick={handleSubmit}
                whileTap={{ scale: 1.3 }}
              >
                {loading ? (
                  <div role="status">
                    <svg
                      aria-hidden="true"
                      className="w-7 h-7 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-black"
                      viewBox="0 0 100 101"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                        fill="currentColor"
                      />
                      <path
                        d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                        fill="currentFill"
                      />
                    </svg>
                    <span className="sr-only">Loading...</span>
                  </div>
                ) : (
                  <p> Sign Up</p>
                )}
              </motion.button>
            </div>
            {error ? (
              <>
                <p className="flex pl-3 text-center mt-4 font-medium bg-red-400 text-white rounded-full ml-14 mr-14 pt-1 pb-1">
                  Please fill the required fields
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={2.5}
                    stroke="currentColor"
                    className="w-5 h-5 mt-1 ml-5 cursor-pointer"
                    onClick={() => setError(false)}
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M6 18L18 6M6 6l12 12"
                    />
                  </svg>
                </p>
              </>
            ) : (
              <></>
            )}
            {otpError ? (
              <>
                <p className="flex pl-3 text-center mt-4 font-medium bg-red-400 text-white rounded-full ml-14 mr-14 pt-1 pb-1">
                  Error Occured Try Again
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={2.5}
                    stroke="currentColor"
                    className="w-5 h-5 mt-1 ml-12 cursor-pointer"
                    onClick={() => setOtpError(false)}
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M6 18L18 6M6 6l12 12"
                    />
                  </svg>
                </p>
              </>
            ) : (
              <></>
            )}
            {passError ? (
              <>
                <p className="flex pl-3 text-center mt-4 font-medium bg-red-400 text-white rounded-full ml-14 mr-14 pt-1 pb-1">
                  Password does not match
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={2.5}
                    stroke="currentColor"
                    className="w-5 h-5 mt-1 ml-10 cursor-pointer"
                    onClick={() => setPassError(false)}
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M6 18L18 6M6 6l12 12"
                    />
                  </svg>
                </p>
              </>
            ) : (
              <></>
            )}
            <OTPModal
              modaltoggle={modaltoggle}
              onCloseModal={onCloseModal}
              otp1={otp1}
              setOtp1={setOtp1}
              otp2={otp2}
              setOtp2={setOtp2}
              otp3={otp3}
              setOtp3={setOtp3}
              otp4={otp4}
              setOtp4={setOtp4}
              handleOtpCheck={handleOtpCheck}
              modalLoading={modalLoading}
            />
          </div>
          <div className="ml-72 mt-4">
            <Image src="/icons/signup.png" alt="N" width="750" height="400" />
          </div>
        </div>
      </div>
    </>
  );
};

export default Signup;
