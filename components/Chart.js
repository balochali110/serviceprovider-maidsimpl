import React, { useEffect, useRef } from "react";

const WeeklyBarChart = ({ data }) => {
  const chartRef = useRef(null);

  useEffect(() => {
    const drawChart = () => {
      const canvas = chartRef.current;
      const context = canvas.getContext("2d");

      // Set chart dimensions
      const chartWidth = canvas.width;
      const chartHeight = canvas.height;
      const numWeeks = data.length;
      const numBarsPerWeek = data[0].values.length;
      const barGap = 8; // Gap between the bars
      const weekGap = 13; // Gap between the weeks
      const barMaxHeight = chartHeight - 40; // Maximum bar height
      const borderRadius = 7; // Rounded corner radius

      // Clear the canvas
      context.clearRect(0, 0, chartWidth, chartHeight);

      // Calculate the total gaps and available width
      const totalBarGapWidth = numWeeks * (numBarsPerWeek - 1) * barGap;
      const totalWeekGapWidth = (numWeeks - 1) * weekGap;
      const availableWidth =
        chartWidth - totalBarGapWidth - totalWeekGapWidth - barGap; // Subtract barGap to account for the gap at the end

      // Calculate the bar width based on the available width and number of bars
      const barWidth = (availableWidth / (numWeeks * numBarsPerWeek)) * 0.7; // Adjust the ratio to change the width of the bars

      // Find the maximum value in the data
      const maxDataValues = data.map((weekData) =>
        Math.max(...weekData.values)
      );

      // Calculate the height scale for the bars
      const heightScale = barMaxHeight / Math.max(...maxDataValues);

      // Draw the bars
      data.forEach((weekData, weekIndex) => {
        const weekX =
          weekIndex * (numBarsPerWeek * (barWidth + barGap + weekGap));

        weekData.values.forEach((barValue, barIndex) => {
          const barHeight = barValue * heightScale;
          const barX = weekX + barIndex * (barWidth + barGap);
          const barY = chartHeight - barHeight;

          // Customize the bar color
          if (barValue === maxDataValues[weekIndex]) {
            context.fillStyle = "#8cd790"; // Change the color for the highest bar in each week
          } else {
            context.fillStyle = "#4A4A48";
          }

          // Draw the rounded rectangle bar
          context.beginPath();
          context.moveTo(barX + borderRadius, barY);
          context.lineTo(barX + barWidth - borderRadius, barY);
          context.quadraticCurveTo(
            barX + barWidth,
            barY,
            barX + barWidth,
            barY + borderRadius
          );
          context.lineTo(barX + barWidth, barY + barHeight - borderRadius);
          context.quadraticCurveTo(
            barX + barWidth,
            barY + barHeight,
            barX + barWidth - borderRadius,
            barY + barHeight
          );
          context.lineTo(barX + borderRadius, barY + barHeight);
          context.quadraticCurveTo(
            barX,
            barY + barHeight,
            barX,
            barY + barHeight - borderRadius
          );
          context.lineTo(barX, barY + borderRadius);
          context.quadraticCurveTo(barX, barY, barX + borderRadius, barY);
          context.closePath();
          context.fill();
        });
      });
    };

    drawChart();
  }, [data]);

  return (
    <div className="flex place-content-center ml-24">
      <canvas ref={chartRef} width={1300} height={300} />;
    </div>
  );
};

export default WeeklyBarChart;
