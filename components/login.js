import React, { useState, useEffect } from "react";
import Image from "next/image";
import { useRouter } from "next/router";
import { useSession, signIn, signOut, getProviders } from "next-auth/react";
import { motion } from "framer-motion";
import { setToken, apiRequest } from "../lib/auth";

const Login = () => {
  const router = useRouter();
  // const { data: session, status } = useSession();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);

  // const [facebook, setFacebook] = useState("");
  // const [google, setGoogle] = useState("");

  // const providers = async () => {
  //   const prod = await getProviders();
  //   setFacebook(prod.facebook);
  //   setGoogle(prod.google);
  // };

  // if (status === "authenticated") {
  //   signOut();
  // }
  // const handleGoogleSignin = (e) => {
  //   e.preventDefault();
  //   signIn(google.id);
  // };

  // const handleFacebookSignIn = (e) => {
  //   e.preventDefault();
  //   signIn(facebook.id)
  // }

  const [error, setError] = useState(false);
  const handleLogin = async (e) => {
    e.preventDefault();
    setLoading(true);
    const data = {
      email: email,
      password: password,
    };

    const res = await fetch("/api/login", {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify(data),
    });

    const result = await res.json();

    if (result.token) {
      const response = await apiRequest("/api/login", "POST", data);
      if (response.token) {
        setLoading(false);
        setToken(response.token, data.email);
        router.push({
          pathname: "/home",
        });
      }
    } else {
      setError(true);
      setLoading(false);
    }
  };

  const handleSignUp = () => {
    router.push("/signup");
  };
  return (
    <>
      <div className="pl-44 pt-12 bg-[#F7F7F7] pb-8">
        <div className="flex">
          <Image
            src="/img/main-logo.png"
            alt="N"
            width="400"
            height="300"
            className="w-56 cursor-pointer"
            onClick={() => router.push("/home")}
          />
        </div>
        <div className="flex mt-6">
          <div>
            <p className="text-5xl font-semibold mt-6 tracking-wide">
              Welcome Back
            </p>
            <p className="text-xl mt-6">Enter your details below</p>
            <form action="" className="mt-10">
              <div>
                <p className="text-lg font-semibold">Email Address</p>
                <input
                  type="text"
                  className="mt-4 w-96 h-12 rounded-full border border-2 bg-transparent pl-8 font-semibold pr-8 tracking-widest"
                  placeholder=""
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </div>
              <div className="mt-4">
                <p className="text-lg font-semibold">Password</p>
                <input
                  type="password"
                  className="mt-4 w-96 h-12 rounded-full border border-2 bg-transparent pl-8 font-semibold pr-8 tracking-widest"
                  placeholder=""
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
              </div>
              <div className="pt-4 flex place-content-center">
                <motion.button
                  className="px-32 py-4 text-center text-xl text-white rounded-full bg-[#4A4A48] font-semibold"
                  onClick={handleLogin}
                  whileTap={{ scale: 0.7 }}
                >
                  {loading ? (
                    <div role="status">
                      <svg
                        aria-hidden="true"
                        className="w-7 h-7 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-black"
                        viewBox="0 0 100 101"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                          fill="currentColor"
                        />
                        <path
                          d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                          fill="currentFill"
                        />
                      </svg>
                      <span className="sr-only">Loading...</span>
                    </div>
                  ) : (
                    <p> Sign In</p>
                  )}
                </motion.button>
              </div>
              <p className="text-center text-gray-600 text-lg mt-2">
                Forgot Passward?
              </p>
              {error ? (
                <p className="mr-14 ml-14 text-center text-white text-base mt-4 bg-red-800 py-1 font-medium rounded-full">
                  Invalid Credentials
                </p>
              ) : (
                ""
              )}
              <p className="text-lg mt-5 font-normal">Or continue with</p>
              <div className="flex mt-8 place-content-center">
                <div className="px-4 py-4 border border-2 rounded-lg">
                  <Image src="/icons/apple.png" alt="" width="50" height="50" />
                </div>
                <div className="px-4 py-4 border border-2 rounded-lg ml-9">
                  <Image
                    src="/icons/google.png"
                    alt=""
                    width="50"
                    height="50"
                  />
                </div>
                <div className="px-4 py-4 border border-2 rounded-lg ml-9">
                  <Image
                    src="/icons/facebook.png"
                    alt=""
                    width="50"
                    height="50"
                  />
                </div>
              </div>
            </form>
            <div className="flex place-content-center mt-9 mb-3">
              <span>
                <p className="text-xl font-semibold ml-3">
                  Don't have an account?
                </p>
                <span className="flex place-content-center">
                  <motion.button
                    className="mt-2 px-5 py-2 font-semibold rounded-lg bg-[#E8E8E8]"
                    onClick={handleSignUp}
                    whileTap={{ scale: 0.5 }}
                  >
                    Get Started
                  </motion.button>
                </span>
              </span>
            </div>
          </div>
          <div className="w-1/2 mt-8 ml-72">
            <Image src="/img/laptop.png" alt="N" width="1400" height="1300" />
          </div>
        </div>
      </div>
    </>
  );
};

export default Login;
