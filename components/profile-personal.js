import React, { useState } from "react";
import ProfileUpdate from "./ProfileUpdate";

const PersonalInformation = () => {

  const [modalToggle, setModalToggle] = useState(false)

  const onCloseModal = () => {
    setModalToggle(false)
  }

  const handleModal = () => {
    setModalToggle(true)
  }
  return (
    <>
      <div className="mt-12 text-xl font-semibold w-full rounded-lg pt-2">
        <div className="border border-1 bg-[#8cd790] pt-3 mr-56 border-black rounded-lg mb-16">
          <div className="flex w-full mb-4 pl-8 text-white">
            <p className="w-5/6">Personal Information</p>
          </div>
          <div className="bg-white pt-1 pl-8 pb-8">
            {/* Name */}
            <div className="mt-12 flex w-full">
              <span className="w-1/4">
                <p className="text-base text-gray-400">First Name</p>
                <p className="text-lg text-black font-semibold">John</p>
              </span>
              <span className="">
                <p className="text-base text-gray-400">Last Name</p>
                <p className="text-lg text-black font-semibold">Smith</p>
              </span>
            </div>
            {/* Email */}
            <div className="mt-12 flex w-full">
              <span className="w-1/4">
                <p className="text-base text-gray-400">Email address</p>
                <p className="text-lg text-black font-semibold">
                  johnsmith@gmail.com
                </p>
              </span>
              <span className="">
                <p className="text-base text-gray-400">Phone</p>
                <p className="text-lg text-black font-semibold ">94527521365</p>
              </span>
            </div>
          </div>
          <div className="mt-2 ml-12 text-white mb-2">
            <p className="w-5/6">Address</p>
          </div>
          <div className="bg-white rounded-b-lg pb-4 pt-2">
            {" "}
            {/* Name */}
            <div className="mt-12 ml-12 flex w-full">
              <span className="w-1/4">
                <p className="text-base text-gray-400">Country</p>
                <p className="text-lg text-black font-semibold">
                  United States
                </p>
              </span>
              <span className="">
                <p className="text-base text-gray-400">City</p>
                <p className="text-lg text-black font-semibold">Orlando</p>
              </span>
            </div>
            {/* Email */}
            <div className="mt-12 ml-12 flex w-full">
              <span className="w-1/4">
                <p className="text-base text-gray-400">Postal Code</p>
                <p className="text-lg text-black font-semibold">32789</p>
              </span>
            </div>
            <div className="flex place-content-end mr-10">
              <div className="cursor-pointer ml-28">
                <p className="flex rounded-full w-full border pb-1 text-base border-1 border-black px-4 text-black font-semibold pt-1" onClick={handleModal}>
                  Edit
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="currentColor"
                    className="w-5 h-5 ml-2"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10"
                    />
                  </svg>
                </p>
                <ProfileUpdate modalToggle={modalToggle} onCloseModal={onCloseModal} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default PersonalInformation;
