import React, { useState } from "react";
import Select from "react-select";

const Orders = () => {
  const options = [
    { value: "upcoming", label: "Upcoming" },
    { value: "confrimed", label: "Confirmed" },
    { value: "completed", label: "Completed" },
  ];

  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <>
      <div className="pt-56 bg-white pb-12">
        <div className="flex place-content-center">
          <div className="w-4/6">
            <p className="text-center ml-56 text-4xl font-bold uppercase">
              Booking History
            </p>
          </div>
          <div className="flex place-content-center w-1/6">
            <Select
              defaultValue={selectedOption}
              onChange={setSelectedOption}
              options={options}
              className="w-56"
            />
          </div>
        </div>

        <div className="flex place-content-center">
          <div className="bg-gray-300 mt-12 w-4/6 pt-6 pl-6 pr-6 pb-6 rounded-lg">
            <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
              <thead className="text-xs text-gray-700 uppercase bg-gray-200 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                  <th scope="col" className="px-6 py-3">
                    DATE
                  </th>
                  <th scope="col" className="px-6 py-3">
                    ADDRESS
                  </th>
                  <th scope="col" className="px-6 py-3">
                    SERVICE
                  </th>
                  <th scope="col" className="px-6 py-3">
                    TEAM ASSIGNED
                  </th>
                  <th scope="col" className="px-6 py-3">
                    PAYMENT
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr className="bg-white border-b text-base dark:bg-gray-800 dark:border-gray-700">
                  <td className="px-6 py-4">19/10/2022</td>
                  <td className="px-6 py-4 w-1/5">Lorem ipsum dolor sit amet, consectetur </td>
                  <td className="px-6 py-4">Regular</td>
                  <td className="px-6 py-4">ABC</td>
                  <td className="px-6 py-4">$ 100</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
          <br />
          <br />
      </div>
    </>
  );
};

export default Orders;
