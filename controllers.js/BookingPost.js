import Booking from "../schema/BookingSchema.js";

export default async function BookingPost(req, res) {
  try {
    const {
      servicetype,
      home,
      extras,
      date,
      time,
      howoften,
      firstname,
      lastname,
      email,
      contact,
      address,
      aptsuite,
      city,
      state,
      zipcode,
      comments,
    } = req.body;

    const booking = new Booking({
      servicetype: servicetype,
      home: home,
      extras: extras,
      date: date,
      time: time,
      howoften: howoften,
      firstname: firstname,
      lastname: lastname,
      email: email,
      contact: contact,
      address: address,
      aptsuite: aptsuite,
      city: city,
      state: state,
      zipcode: zipcode,
      comments: comments,
    });

    const saveBooking = await booking.save();
    res.status(200).json({ result: "Booking Done" });
  } catch (err) {
    res.status(200).json({ error: err });
  }
}
