import jwt from "jsonwebtoken";

export default function handler(req, res) {
  const token = req.cookies.token;

  if (!token) {
    return res.status(401).json({ message: "Unauthorized" });
  }

  try {
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const username = decoded.username;
    const data = username;
    res.status(200).json({ data });
  } catch (error) {
    res.status(401).json({ message: "Invalid token" });
  }
}
