import Signup from "../schema/SignUpSchema";
export default async function getUsers (req, res) {
    Signup.find({}).then((response) => {
        res.status(200).json({
            success: true, 
            result: response
        })
    }).catch((err) => {
        res.status(422).json({
            error: err
        })
    })
}