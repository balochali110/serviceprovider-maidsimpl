import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import Signup from "../schema/SignUpSchema";
import Joi from "@hapi/joi";
import { serialize } from "cookie";

const loginSchema = Joi.object({
  email: Joi.string().min(6).required().email(),
  password: Joi.string().min(6).required(),
});

export default async function Login(req, res) {
  const user = await Signup.findOne({ email: req.body.email });
  if (!user) {
    return res.status(400).send({ error: "incorrect email" });
  }

  const validPassword = await bcrypt.compare(req.body.password, user.password);
  if (!validPassword)
    return res.status(400).send({ error: "incorrect password" });

  try {
    const { error } = await loginSchema.validateAsync(req.body);
    if (error) return res.status(400).send({ error: "error occured" });
    else {
      const payload = {
        id: user._id,
        email: user.email,
      };

      const token = jwt.sign(payload, process.env.JWT_SECRET);
      res.status(200).json({ token });
    }
  } catch (err) {
    res.status(500).send({ error: "error occured" });
  }
}
