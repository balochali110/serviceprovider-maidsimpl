import { Schema, model, models } from "mongoose";

const BookingSchema = new Schema({
  servicetype: {
    type: String,
  },
  home: {
    type: Array,
  },
  extras: {
    type: Array,
  },
  date: {
    type: String,
  },
  time: {
    type: String,
  },
  howoften: {
    type: String,
  },
  firstname: {
    type: String,
  },
  lastname: {
    type: String,
  },
  email: {
    type: String,
  },
  contact: {
    type: String,
  },
  address: {
    type: String,
  },
  aptsuite: {
    type: String,
  },
  city: {
    type: String,
  },
  state: {
    type: String,
  },
  zipcode: {
    type: Number,
  },
  comments: {
    type: String,
  },
});

const Booking = models.Booking || model("Booking", BookingSchema);
export default Booking;
