import React from "react";
import Head from "next/head";
import ViewDetails from "@/components/viewDetails";
import Navbar from "@/components/navbar";

const viewDetails = () => {
  return (
    <div>
      <Head>
        <title>Maid Simpl</title>
        <meta
          name="description"
          content="Nextly is a free landing page template built with next.js & Tailwind CSS"
        />
        <link rel="icon" href="/img/favicon.png" />
      </Head>
      <Navbar />
      <div className="bg-white">
        <ViewDetails />
      </div>
    </div>
  );
};

export default viewDetails;
