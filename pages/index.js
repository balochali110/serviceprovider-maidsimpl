import Head from "next/head";
import { useRouter } from "next/router";

const Home = () => {
  const router = useRouter()
  setTimeout(() => {
    router.push("/home")
  }, 5000)
  return (
    <>
      <Head>
        <title>Maid Simpl</title>
        <link rel="icon" href="/img/favicon.png" />
      </Head>

      <div>
        <video autoPlay muted>
          <source src="/video/maidsimple.mp4" type="video/mp4" />
          Sorry, your browser doesn't support videos.
        </video>
      </div>
    </>
  );
};

export default Home;
