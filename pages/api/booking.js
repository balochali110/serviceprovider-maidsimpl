import BookingPost from "../../controllers.js/BookingPost.js";
import connection from "../../lib/connection.js";

const booking = async (req, res) => {
  await connection();
  const method = req.method;
  switch (method) {
    case "POST":
      let result = await BookingPost(req, res);
      break;
  }
};

export default booking;
