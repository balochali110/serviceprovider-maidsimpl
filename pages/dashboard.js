import React from "react";
import Head from "next/head";
import Navbar from "@/components/navbar";
import Footer from "@/components/footer";
import HistoryBooking from "@/components/HistoryBooking";
const historybooking = () => {
  return (
    <>
      <Head>
        <title>Maid Simpl</title>
        <meta
          name="description"
          content="Nextly is a free landing page template built with next.js & Tailwind CSS"
        />
        <link rel="icon" href="/img/favicon.png" />
      </Head>
      <Navbar />
      <HistoryBooking />
      <Footer />
    </>
  );
};

export default historybooking;
