import React from "react";
import Head from "next/head";
import Footer from "@/components/footer";
import Navbar from "@/components/navbar";
import MyCards from "@/components/MyCards";
const Cards = () => {
  return (
    <>
      <Head>
        <title>Maid Simpl</title>
        <meta
          name="description"
          content="Nextly is a free landing page template built with next.js & Tailwind CSS"
        />
        <link rel="icon" href="/img/favicon.png" />
      </Head>
      <Navbar />
      <MyCards />
      <Footer />
    </>
  );
};

export default Cards;
